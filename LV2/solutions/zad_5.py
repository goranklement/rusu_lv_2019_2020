import pandas as pd
import matplotlib.pyplot as plt
fname = input('Enter the file name: ')
csvData = pd.read_csv(fname)
csvData.sort_values(csvData.columns[4], 
                    axis=0,
                    inplace=True)
print(csvData)
mpg=csvData.loc[:,"mpg"]        
hp= csvData.loc[:,"hp"]
wt= csvData.loc[:,"wt"]
df = pd.DataFrame(csvData)
cols = [1,2,3,4,5,6,7,8,9,10,11]
df = df[df.columns[cols]]
df = df[["hp"]]
plt.plot(hp, mpg, '--', linewidth=1, ) 
plt.xlabel('hp') 
plt.ylabel('mpg')
plt.plot(hp, wt, '*', linewidth=2)

print("Min: " + str(mpg.min())+ ", max: " + str(mpg.max())+ ", avg: " + str(mpg.mean()))


