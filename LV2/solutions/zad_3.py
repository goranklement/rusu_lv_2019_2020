import numpy as np
import matplotlib.pyplot as plt

sex= np.random.randint(2, size=20)
males= np.count_nonzero(sex)
females= len(sex)-males

mu_male, sigma_male = 180, 7 
mu_female, sigma_female= 167, 7
 
male = np.random.normal(mu_male, sigma_male, males)
female= np.random.normal(mu_female, sigma_female, females)
avg_males= np.mean(males)
avg_females= np.mean(females)
plt.plot(male,  'b-', linewidth=2)
plt.plot(female, 'r-', linewidth=2)



