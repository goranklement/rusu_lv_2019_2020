fname = input('Enter the file path: ')  
try:
    fhand = open(fname)
except:
    print ("File cannot be opened: " +fname)
    exit()
numbers= []
for line in fhand: 
 if line.startswith("X-DSPAM-Confidence:"): 
    number= line[20:-1]
    numbers.append(float(number))
print("Average X-DSPAM-Confidence: " + str(sum(numbers)/len(numbers)))