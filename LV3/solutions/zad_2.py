import pandas as pd
import matplotlib.pyplot as plt
#1st part
mtcars = pd.read_csv(r'C:\Users\Matea\Desktop\rusu_lv_2019_2020\LV3\resources\mtcars.csv')
mtcars_cyl = mtcars.groupby('cyl')
mtcars_mpg= mtcars_cyl.mpg.mean()
mtcars_mpg.plot.bar()
plt.ylabel("Average MPG")
plt.title("Fuel consumption")

#2nd part
cyl4= mtcars[mtcars.cyl == 4]
cyl6= mtcars[mtcars.cyl == 6]
cyl8= mtcars[mtcars.cyl == 8]
cyl4_weight=[]
cyl6_weight=[]
cyl8_weight=[]
for i in cyl4["wt"]:
    cyl4_weight.append(i)

for i in cyl6["wt"]:
    cyl6_weight.append(i)

for i in cyl8["wt"]:
    cyl8_weight.append(i)

plt.figure()
plt.boxplot([ cyl4_weight, cyl6_weight,  cyl8_weight ], positions = [4,6,8])
plt.xlabel('Cylinders')
plt.ylabel('Weigth in ton')

#3rd part
plt.figure()
mtcars_am= mtcars.groupby('am')
mtcars_mpg2= mtcars_am.mpg.mean()
mtcars_mpg2.plot.bar()
plt.xlabel("Automatic")
plt.ylabel("Average mpg")
plt.title("Automatic vs manual fuel consumption")

#4th part
acceleration_a=[]
hp_a=[]
hp_m=[]
acceleration_m=[]
automatic=mtcars[(mtcars.am== 1)]
manual=mtcars[(mtcars.am== 0)]

for i in automatic["qsec"]:  
    acceleration_a.append(i)
    
for i in automatic["hp"]:  
    hp_a.append(i)
    
for i in manual["hp"]:  
    hp_m.append(i)    
for i in manual["qsec"]:  
    acceleration_m.append(i)
    
plt.figure()
plt.scatter(acceleration_a, hp_a, color="DarkBlue", label="Automatic")
plt.scatter(acceleration_m, hp_m, color="DarkGreen", label="Manual");
plt.xlabel("acceleration")
plt.ylabel("HP")
plt.title("Manual vs Automatic")
plt.legend(["Automatic","Manual"])

