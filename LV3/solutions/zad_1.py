import pandas as pd
mtcars = pd.read_csv(r'C:\Users\Matea\Desktop\rusu_lv_2019_2020\LV3\resources\mtcars.csv')
mtcars.sort_values(mtcars.columns[1], axis=0,
                    inplace=True)
print(mtcars.head(5))
print(mtcars[mtcars.cyl == 8].head(3)) 
print(mtcars[mtcars.cyl== 6].mpg.mean())
print("Automatic: " + str(len(mtcars[mtcars.am== 1])) + ", manual: " + str(len(mtcars[mtcars.am== 0])))
print("AM and >100hp: " + str(len(mtcars[(mtcars.am==1) & (mtcars.hp>100)])))
mtcars['tons'] = mtcars.wt*1000
print(mtcars[["car", "tons"]]) 