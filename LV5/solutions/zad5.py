import scipy as sp 
from sklearn import cluster
import numpy as np 
import matplotlib.image as mpimg 
import matplotlib.pyplot as plt 
try: 
 face = sp.face(gray=True) 
except AttributeError: 
 from scipy import misc 
 face = misc.face(gray=True) 

imageNew = mpimg.imread(r'C:\Users\goran\Desktop\rusu_lv_2019_2020\LV5\resources\example.png')
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array 
k_means = cluster.KMeans(n_clusters=3,n_init=1) 
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze() 
labels = k_means.labels_ 
new_compressed = np.choose(labels, values) 
new_compressed.shape = imageNew.shape 
plt.figure(1) 
plt.imshow(imageNew, cmap='gray') 
plt.figure(2) 
plt.imshow(new_compressed, cmap='gray')
