from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.datasets import make_circles
from sklearn.datasets import make_moons
import matplotlib.pyplot as plt
import numpy as np
def generate_data(n_samples, flagc):
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
        cluster_std=[1.0, 2.5, 0.5],
        random_state=random_state)
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    else:
        X = []
    return X

for i in range (1,6):
    X=generate_data(500,i)
    kmeans= KMeans(n_clusters= 3)
    kmeans.fit(X)
    group=kmeans.predict(X)
    centers=kmeans.cluster_centers_
    plt.scatter(X[:, 0], X[:, 1], c=group, s=50, cmap='viridis')
    plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.6);
    plt.show()

criteria_function=[]
clusters=[]
for i in range(1,20):
    kmeans=KMeans(n_clusters=i)
    kmeans.fit(X)
    clusters.append(i)
    criteria_function.append(kmeans.inertia_)
plt.figure()
plt.plot(clusters, criteria_function)
plt.xticks(np.arange(min(clusters), max(clusters)+2, 1))
plt.grid(linestyle='--')